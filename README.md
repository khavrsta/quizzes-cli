# Quizzes CLI
Autor: Stanislav Khavruk

## Téma z Progtestu

Kvízy

Vytvořte program, který umožní vytvářet kvízy a testovat znalosti.

Implementujte následující funkcionality:

1. alespoň 3 různé druhy otázek (př. textová, single-choice, multi-choice)
2. alespoň 3 různé druhy vyhodnocení odpovědi na jeden konkrétní druh otázky (na zbylé druhy otázek  může být 1 či více druhů odpovědi, př. exact-match, pattern, set)
3. implementujte stránky kvízu (na stránce je více otázek, kvíz má více stránek)
4. implementujte přechod mezi stránkami sekvenčně a v závislosti na odpovědi (př. otázka "Jaký je váš věk?" Při odpovědi 30 a víc přesměrujeme na poděkování. Jinak vyplnění další stránky s otázkami.)
5. uživatelské rozhraní pro tvorbu a vyplnění (a ohodnocení) kvízu
6. export a import kvízu (a více kvízů)


Použití polymorfismu (doporučené)

- druhy otázek: textová otázka, výběr jednoho, výběr více možností, seřazení možností, ...
- druhy odpovědí: textová odpověď (jedna možnost), více správných textových odpovědí, vypočtená hodnota, ...
- různé formáty: vykreslení do konzole, textový (tisknutelný - bez importu, tisknutelný s výsledky - bez importu, s možností importu zpět), binární, ...


Poznámky k úloze:

- Všimněte si použití polymorfismu, kde jsou ukázky v čem se liší otázka a v čem odpověď.
- Zaměřte se více na pochopení úlohy z hlediska "tvůrce kvízů" (pro něj není otázka jen samotný "text", ale i forma odpovědi).
- Typem vyhodnocení odpovědi je pak například pro textovou otázku (otázku s otevřenou odpovědí):
    + exact-match; konkrétní text (uživatel musí zadat: "červená"),
    + pattern; text splňující regex ("červen*", uživatel musí zadat: "červená", "červené", "červenou", "červenat", ...),
    + set; z množiny (uživatel musí zadat: "červená", "modrá" nebo "bílá"),
    + text musí obsahovat slovo (uživatel musí zadat: "červenou barvu", "vlajka má červenou", "červenou", ...),
    + kombinace více kritérií.

## Specifikace
Budeme implementovat CLI (Command Line Interface) variantu kvízů.
Aplikace má pár různých interface/menu, ve kterých pohyb probíhá přes výběr jednoho bodu (čísla) menu.
Výstupní řádek ">>>" znamená na očekávaní vstupu od uživatele.
Přes aplikace uživatel může:
- Spustit kvíz
- Vytvořit a modifikovat kvíz a jeho komponenty
- Smazat kvíz

Kvíz obsahuje 4 typy otázek:
1. Info — prostě ukazuje text. Funguje jako "MessageBox" pro zobrazení nějakou informaci mezi otázkami.
2. Single choice — umožnuje výběr jenom jednou odpovědi.
3. Multi choice — výběr několik variantů.
4. Text field — odpověď se vstupem od uživatele. Může mít v sobě řadu pravidel pro validaci.

Validation Rules:
- obyčejné operátory porovnávání (<, >, ==, !=, <=, >=)
- contains (text musí obsahovat slovo)
- pattern (text splňující regex)

### Polymorfismus
Polymorfismus je aplikovaný v rámci hierarchie tříd `BaseQuestion`, který má potomky `InfoQuestion`, `MultiChoiceQuestion`, `SingleChoiceQuestion`, `TextQuestion`.
Jejích hlavní polymorfní metody jsou `Render()` a `AwaitAnswer()`, které je používané pro zobracení a zpracování odpovědi na otázku.
Také je často používané virtuálně metody `Validate()` a `Serialize()`. První odpovídá za ověřeni odpovědi, a druhý pro převod otázky v řádek pro zápis do souboru.

Plánoval jsem taký použit polymorfismus i pro Editory, ale nešlo to s použiti šablon (problém s kompilaci).
Muselo se stačit, aby `BaseEditor` měl kolekci (vector) objektu `T` a virtuální metody CRUD a Get/Set.
Nyní, BaseEditor vystoupá v roli pseudo-interfacu pro jíně editory.

Část polymorfismusu byla také použita i pro odpovědi v třidách `BaseAnswer` & `TextAnswer`, aby byla možnost mít nejen pole otázek, a i pole odpověď. + Serialize metody.
