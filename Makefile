CXX = g++
CXX_FLAGS = -Wall -pedantic -g -std=c++17

HEADERS = $(wildcard src/*.h) $(wildcard src/Utils/*.h) $(wildcard src/Answers/*.h) $(wildcard src/Questions/*.h) $(wildcard src/Editors/*.h) $(wildcard src/Answers/Validation/*.h)
SOURCES = $(wildcard src/*.cpp) $(wildcard src/Utils/*.cpp) $(wildcard src/Answers/*.cpp) $(wildcard src/Questions/*.cpp) $(wildcard src/Editors/*.cpp) $(wildcard src/Answers/Validation/*.cpp)
OBJECTS = $(SOURCES:src/%.cpp=build/%.o)
#TESTS = $(wildcard tests/*.test.cpp)

.PHONY: all
all: compile doc

.PHONY: run
run: compile
	./khavrsta

.PHONY: compile
compile: khavrsta

khavrsta: build/main
	cp $< $@

build/main: $(OBJECTS)
	mkdir -p $(@D)
	g++ $(CXX_FLAGS) $^ -o $@ -lstdc++fs

build/%.o: src/%.cpp
	mkdir -p $(@D)
	g++ $(CXX_FLAGS) $< -c -o $@ -lstdc++fs

#debug/%.test: tests/%.test.cpp $(filter-out build/main.o,$(OBJECTS))
#	mkdir -p $(@D)
#	g++ $(CXX_FLAGS) $< $(filter-out build/main.o,$(OBJECTS)) -I src/ -o $@

doc: Doxyfile $(HEADERS)
	doxygen Doxyfile

.PHONY: clean
clean:
	rm -rf khavrsta build/ debug/ doc/ data/ 2>/dev/null

#.PHONY: test_all
#test_all: $(TESTS:tests/%.test.cpp=debug/%.test)
#	for TEST in debug/*.test; do ./$$TEST; done

# Requirements / Dependencies
build/InputManipulator.o: src/Utils/InputManipulator.cpp src/Utils/InputManipulator.h
build/ValidationRule.o: src/Answers/Validation/ValidationRule.cpp src/Answers/Validation/ValidationRule.h \
 src/Utils/ValidationRuleType.h \
 src/Application.h src/Application.cpp
build/BaseAnswer.o: src/Answers/BaseAnswer.cpp src/Answers/BaseAnswer.h
build/TextAnswer.o: src/Answers/TextAnswer.cpp  src/Answers/TextAnswer.h \
 src/Answers/BaseAnswer.cpp src/Answers/BaseAnswer.h \
 src/Answers/Validation/ValidationRule.cpp src/Answers/Validation/ValidationRule.h \
 src/Utils/InputManipulator.cpp src/Utils/InputManipulator.h
build/BaseQuestion.o: src/Questions/BaseQuestion.cpp src/Questions/BaseQuestion.h \
 src/Answers/BaseAnswer.cpp src/Answers/BaseAnswer.h
build/InfoQuestion.o: src/Questions/InfoQuestion.cpp src/Questions/InfoQuestion.h \
 src/Questions/BaseQuestion.cpp src/Questions/BaseQuestion.h
build/SingleChoiceQuestion.o: src/Questions/SingleChoiceQuestion.cpp src/Questions/SingleChoiceQuestion.h \
 src/Questions/BaseQuestion.cpp src/Questions/BaseQuestion.h \
 src/Utils/InputManipulator.cpp src/Utils/InputManipulator.h
build/MultiChoiceQuestion.o: src/Questions/MultiChoiceQuestion.cpp src/Questions/MultiChoiceQuestion.h \
 src/Questions/BaseQuestion.cpp src/Questions/BaseQuestion.h \
 src/Utils/InputManipulator.cpp src/Utils/InputManipulator.h
build/TextQuestion.o: src/Questions/TextQuestion.cpp src/Questions/TextQuestion.h \
 src/Questions/BaseQuestion.cpp src/Questions/BaseQuestion.h \
 src/Answers/TextAnswer.cpp  src/Answers/TextAnswer.h
build/BaseEditor.o: src/Editors/BaseEditor.cpp src/Editors/BaseEditor.h
build/ValidationRuleEditor.o: src/Editors/ValidationRuleEditor.cpp src/Editors/ValidationRuleEditor.h \
 src/Editors/BaseEditor.cpp src/Editors/BaseEditor.h \
 src/Answers/Validation/ValidationRule.cpp src/Answers/Validation/ValidationRule.h \
 src/Utils/InputManipulator.cpp src/Utils/InputManipulator.h
build/AnswerEditor.o: src/Editors/AnswerEditor.cpp src/Editors/AnswerEditor.h \
 src/Editors/BaseEditor.cpp src/Editors/BaseEditor.h \
 src/Answers/BaseAnswer.cpp src/Answers/BaseAnswer.h \
 src/Utils/AnswerFieldType.h \
 src/Editors/ValidationRuleEditor.cpp src/Editors/ValidationRuleEditor.h \
 src/Answers/TextAnswer.cpp  src/Answers/TextAnswer.h \
 src/Utils/InputManipulator.cpp src/Utils/InputManipulator.h
build/QuestionEditor.o: src/Editors/QuestionEditor.cpp src/Editors/QuestionEditor.h \
 src/Editors/BaseEditor.cpp src/Editors/BaseEditor.h \
 src/Questions/BaseQuestion.cpp src/Questions/BaseQuestion.h \
 src/Questions/InfoQuestion.cpp src/Questions/InfoQuestion.h \
 src/Questions/SingleChoiceQuestion.cpp src/Questions/SingleChoiceQuestion.h \
 src/Questions/MultiChoiceQuestion.cpp src/Questions/MultiChoiceQuestion.h \
 src/Questions/TextQuestion.cpp src/Questions/TextQuestion.h \
 src/Utils/InputManipulator.cpp src/Utils/InputManipulator.h
build/Quiz.o: src/Quiz.cpp src/Quiz.h \
 src/Questions/BaseQuestion.cpp src/Questions/BaseQuestion.h \
 src/Utils/InputManipulator.cpp src/Utils/InputManipulator.h
build/Application.o: src/Application.cpp src/Application.h \
 src/Quiz.cpp src/Quiz.h \
 src/Editors/QuestionEditor.cpp src/Editors/QuestionEditor.h
main.o: src/main.cpp \
 src/Application.cpp src/Application.cpp