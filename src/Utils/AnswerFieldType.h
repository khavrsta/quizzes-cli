//
// Created by stas_ on 12/6/2022.
//

#ifndef QUIZZES_ANSWERFIELDTYPE_H
#define QUIZZES_ANSWERFIELDTYPE_H

enum class AnswerFieldType : char {
    None = 0,
    Info,
    RadioButton,
    CheckBox,
    TextInput
};

#endif //QUIZZES_ANSWERFIELDTYPE_H
