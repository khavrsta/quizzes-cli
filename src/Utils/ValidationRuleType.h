//
// Created by stas_ on 12/6/2022.
//

#ifndef QUIZZES_VALIDATIONRULETYPE_H
#define QUIZZES_VALIDATIONRULETYPE_H

enum class ValidationRuleType : char {
    None = 0,
    Equal,
    NotEqual,
    Less,
    More,
    LessOrEqual,
    MoreOrEqual,
    Contains,
    Pattern
};

#endif //QUIZZES_VALIDATIONRULETYPE_H
