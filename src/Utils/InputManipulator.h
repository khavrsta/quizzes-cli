//
// Created by stas_ on 13/6/2022.
//

#ifndef KHAVRSTA_INPUTMANIPULATOR_H
#define KHAVRSTA_INPUTMANIPULATOR_H

#include <iostream>
#include <string>
using std::cout, std::cin, std::endl, std::string;

class InputManipulator {
public:
    static const string SavesDirectory;
    static int ReadNumber();
    static string ReadLine();
    static int ReadMenuChoice(int maxValue, int minValue = 0);
    static bool DoesSuchFileExist(const string& filename);
    static bool DoesSuchDirectoryExists(const string& filename);
    static string ReadFilename(bool failIfExists = false);
    static string ReadStringFromFile(std::istream& stream);
};


#endif //KHAVRSTA_INPUTMANIPULATOR_H
