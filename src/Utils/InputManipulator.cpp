//
// Created by stas_ on 13/6/2022.
//

#include "InputManipulator.h"
#include <filesystem>
#include <climits>

const string InputManipulator::SavesDirectory = "data";

string InputManipulator::ReadLine() {
    string line;
    cout << ">>>";
    do {
        std::getline(std::cin, line);
    } while (line.empty());
    return line;
}

int InputManipulator::ReadNumber() {
    cout << ">>>";
    int number;
    cin >> number;
    if (cin.eof() || !cin.good()) {
        cin.clear();
        cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        throw std::invalid_argument("Failed to read user input.");
    }
    return number;
}

int InputManipulator::ReadMenuChoice(int maxValue, int minValue) {
    int choice;
    do {
        try {
            choice = InputManipulator::ReadNumber();
        } catch(...) {
            // repeat user input request if entered invalid input (i.e. not digit)
            continue;
        }
    } while (choice < minValue || choice > maxValue);
    return choice;
}

bool InputManipulator::DoesSuchFileExist(const string& filename) {
    std::filesystem::path files = { filename };
    return std::filesystem::exists(files);
}

bool InputManipulator::DoesSuchDirectoryExists(const string& directoryName) {
    return !std::filesystem::is_directory(directoryName) || !std::filesystem::exists(directoryName);
}

string InputManipulator::ReadFilename(bool failIfExists) {
    string filename;
    bool isExists = false;
    bool isDone = false;
    do {
        filename = ReadLine();
        isExists = DoesSuchFileExist(SavesDirectory + "/" + filename);
        if (failIfExists && isExists) {
            cout << "Filename \"" << filename << "\" is already exists. Please choose another name." << endl;
            isDone = false;
            continue;
        } else if (!failIfExists && !isExists) {
            cout << "Cannot find file \"" << filename << "\"." << endl;
            isDone = false;
            continue;
        }
        isDone = true;
    } while(filename.empty() || !isDone);
    return filename;
}

string InputManipulator::ReadStringFromFile(std::istream& stream) {
    string line;
    while(line.empty()) {
        if (stream.eof()) {
            throw std::invalid_argument("InputManipulator::ReadStringFromFile(std::istream& stream): Unexpected end of file.");
        }
        else if (!stream.good()) {
            throw std::invalid_argument("InputManipulator::ReadStringFromFile(std::istream& stream): Failed to read from stream.");
        }
        std::getline(stream, line);
    }
    return line;
}


