//
// Created by stas_ on 10/6/2022.
//

#ifndef SRC_QUIZ_H
#define SRC_QUIZ_H

#include "Questions/BaseQuestion.h"
#include <vector>
#include <string>
#include <memory>
using std::vector, std::shared_ptr, std::string;

class Quiz {
private:
    string _filename;
    vector<shared_ptr<BaseQuestion>> _questions;
    std::pair<int, int> _quizScore;
public:
    Quiz();
    Quiz(const string& name);

    [[nodiscard]] vector<shared_ptr<BaseQuestion>> GetAllQuestions() const;
    [[nodiscard]] shared_ptr<BaseQuestion> GetQuestion(int index) const;
    void SetQuestions(const vector<shared_ptr<BaseQuestion>>& questions);
    [[nodiscard]] shared_ptr<Quiz> GetShared() const;
    [[nodiscard]] string GetFilename() const;
    [[nodiscard]] std::pair<int, int> GetScore() const;

    void RenderEntireQuiz();
    void SetFilename(const string& filename);
    void Open(const string& filename);
    void Save(const string& filename);
};


#endif //SRC_QUIZ_H
