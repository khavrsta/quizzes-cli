//
// Created by stas_ on 10/6/2022.
//

#ifndef SRC_SINGLECHOICEQUESTION_H
#define SRC_SINGLECHOICEQUESTION_H

#include "BaseQuestion.h"

class SingleChoiceQuestion : public BaseQuestion {
private:
public:
    SingleChoiceQuestion(const string& text);
    void Render() const override;
    std::pair<int, int> Validate() const override;
    shared_ptr<BaseAnswer> AwaitAnswer() override;
    shared_ptr<BaseQuestion> GetShared() const override;
    void PrintRemarks() const override;
    string Serialize() override;
    [[nodiscard]] AnswerFieldType GetFieldType() const override;
};


#endif //SRC_SINGLECHOICEQUESTION_H
