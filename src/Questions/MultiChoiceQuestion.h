//
// Created by stas_ on 11/6/2022.
//

#ifndef SRC_MULTICHOICEQUESTION_H
#define SRC_MULTICHOICEQUESTION_H

#include "BaseQuestion.h"

class MultiChoiceQuestion : public BaseQuestion {
public:
    MultiChoiceQuestion(const string& text);
    void Render() const override;
    shared_ptr<BaseAnswer> AwaitAnswer() override;
    [[nodiscard]] shared_ptr<BaseQuestion> GetShared() const override;
    [[nodiscard]] std::pair<int,int> Validate() const override;
    void PrintRemarks() const override;
    string Serialize() override;
    [[nodiscard]] AnswerFieldType GetFieldType() const override;
};


#endif //SRC_MULTICHOICEQUESTION_H
