//
// Created by stas_ on 11/6/2022.
//

#include "MultiChoiceQuestion.h"
#include <iostream>
#include <sstream>
#include "../Utils/InputManipulator.h"
using std::cout, std::endl;

MultiChoiceQuestion::MultiChoiceQuestion(const string &text) : BaseQuestion(text) {

}

void MultiChoiceQuestion::Render() const {
    BaseQuestion::Render();
    PrintRemarks();
}

shared_ptr<BaseAnswer> MultiChoiceQuestion::AwaitAnswer() {
    bool isNumbersEntered = false;
    vector<int> selectedAnswers;
    do {
        string line = InputManipulator::ReadLine();
        std::stringstream ss(line);

        int selectedAnswerIndex = -1;
        while (!ss.eof()) {
            if (!(ss >> selectedAnswerIndex)) {
                isNumbersEntered = false;
                cout << "Invalid value. Please enter only answer order numbers." << endl;
                selectedAnswers.clear();
                break;
            }
            if (selectedAnswerIndex < 1 || selectedAnswerIndex > (int) _answers.size()) {
                isNumbersEntered = false;
                cout << "Invalid answer number. Enter number in range from 1 to " << _answers.size() << endl;
                selectedAnswers.clear();
                break;
            }
            selectedAnswers.push_back(selectedAnswerIndex - 1);
            isNumbersEntered = true;
        }
    } while (!isNumbersEntered);

    for(const auto& selected : selectedAnswers) {
        _answers[selected]->Select();
    }
    return nullptr;
}

std::pair<int, int> MultiChoiceQuestion::Validate() const {
    int numberOfValidAnswers = 0;
    int numberOfSelectedValidAnswers = 0;
    for(const auto& answer : _answers) {
        if (answer->IsCorrect()) {
            ++numberOfValidAnswers;
            if (answer->IsSelected()) {
                ++numberOfSelectedValidAnswers;
            }
        }
    }
    return {numberOfSelectedValidAnswers, numberOfValidAnswers};
}

shared_ptr<BaseQuestion> MultiChoiceQuestion::GetShared() const {
    return std::make_shared<MultiChoiceQuestion>(*this);
}

void MultiChoiceQuestion::PrintRemarks() const {
    cout << "You may choose several answers." << endl;
}

string MultiChoiceQuestion::Serialize() {
    string data = "2 " + std::to_string(_answers.size()) + "\n" + _questionText + "\n";
    for(const auto& answer : _answers) {
        data += answer->Serialize();
    }
    return data;
}

AnswerFieldType MultiChoiceQuestion::GetFieldType() const {
    return AnswerFieldType::CheckBox;
}