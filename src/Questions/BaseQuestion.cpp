//
// Created by stas_ on 10/6/2022.
//

#include <iostream>
#include "BaseQuestion.h"
using std::cout, std::endl;

BaseQuestion::BaseQuestion(const string &text) {
    _questionText = text;
    _selectedAnswerIndex = -1;
}

shared_ptr<BaseQuestion> BaseQuestion::AddAnswer(const shared_ptr<BaseAnswer> newAnswer) {
    _answers.emplace_back(newAnswer);
    return this->GetShared();
}

shared_ptr<BaseQuestion> BaseQuestion::GetShared() const {
    return std::make_shared<BaseQuestion>(*this);
}

void BaseQuestion::Render() const {
    cout << _questionText << "\n" << endl;
    int i = 1;
    for(const auto& a : _answers) {
        cout << i++ << ". ";
        a->Render();
    }
}

std::pair<int, int> BaseQuestion::Validate() const {
    return {0, 0};
}

vector<shared_ptr<BaseAnswer>> BaseQuestion::GetAnswers() const {
    return _answers;
}

shared_ptr<BaseAnswer> BaseQuestion::AwaitAnswer() {
    _selectedAnswerIndex = -1;
    return nullptr;
}

shared_ptr<BaseQuestion> BaseQuestion::AddAnswer(const BaseAnswer& newAnswer) {
    _answers.emplace_back(newAnswer.GetShared());
    return this->GetShared();
}

string BaseQuestion::GetText() const {
    return _questionText;
}

void BaseQuestion::PrintRemarks() const {}

void BaseQuestion::SetText(const string &text) {
    _questionText = text;
}

string BaseQuestion::Serialize() {
    return "";
}

BaseQuestion::BaseQuestion() {
    _selectedAnswerIndex = -1;
}

AnswerFieldType BaseQuestion::GetFieldType() const {
    return AnswerFieldType::None;
}

void BaseQuestion::SetAnswers(const vector<shared_ptr<BaseAnswer>>& answers) {
    _answers.clear();
    for(const auto& answer : answers) {
        _answers.push_back(answer);
    }
}
