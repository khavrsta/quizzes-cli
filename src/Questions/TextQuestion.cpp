//
// Created by stas_ on 12/6/2022.
//

#include "TextQuestion.h"
#include "../Answers/TextAnswer.h"
#include "../Utils/InputManipulator.h"
#include <iostream>
#include <sstream>
using std::cout, std::endl;

TextQuestion::TextQuestion(const string &text) : BaseQuestion(text) {
    _answers.push_back(TextAnswer("[User Text Input]").GetShared());
}

shared_ptr<BaseQuestion> TextQuestion::AddAnswer(shared_ptr<BaseAnswer> newAnswer) {
    _answers.clear();
    _answers.push_back(newAnswer);
}

shared_ptr<BaseQuestion> TextQuestion::GetShared() const {
    return std::make_shared<TextQuestion>(*this);
}

void TextQuestion::Render() const {
    cout << _questionText << endl;
    cout << "This is text question — any entered text before Enter press would be account as answer." << endl;
}

shared_ptr<BaseAnswer> TextQuestion::AwaitAnswer() {
    string answer = InputManipulator::ReadLine();
    auto textAnswer = std::dynamic_pointer_cast<TextAnswer>(_answers[0]);
    textAnswer->SetValue(answer);
    return textAnswer;
}

void TextQuestion::PrintRemarks() const {
    cout << "This is text question." << endl;
    auto textAnswer = std::dynamic_pointer_cast<TextAnswer>(_answers[0]);
    textAnswer->ShowAllValidationRules();
}

std::pair<int, int> TextQuestion::Validate() const {
    if (_answers[0]->IsCorrect()) {
        return {1, 1};
    }
    return {0, 1};
}

string TextQuestion::Serialize() {
    if (_answers.empty())
        return "3 0\n";
    auto textAnswer = std::dynamic_pointer_cast<TextAnswer>(_answers[0]);
    string data = "3 1\n" + _questionText + "\n";
    data += std::to_string(textAnswer->GetAllRules().size()) + "\n";
    for(const auto& rule : textAnswer->GetAllRules()) {
        data += rule.Serialize();
    }
    return data;
}

AnswerFieldType TextQuestion::GetFieldType() const {
    return AnswerFieldType::TextInput;
}
