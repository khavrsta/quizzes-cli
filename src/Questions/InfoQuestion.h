//
// Created by stas_ on 10/6/2022.
//

#ifndef SRC_INFOQUESTION_H
#define SRC_INFOQUESTION_H

#include "BaseQuestion.h"

class InfoQuestion : public BaseQuestion {
public:
    InfoQuestion(const string& text);
    void Render() const override;
    shared_ptr<BaseAnswer> AwaitAnswer() override;
    [[nodiscard]] shared_ptr<BaseQuestion> GetShared() const override;
    void PrintRemarks() const override;
    string Serialize() override;
    [[nodiscard]] AnswerFieldType GetFieldType() const override;
};


#endif //SRC_INFOQUESTION_H
