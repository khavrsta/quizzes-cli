//
// Created by stas_ on 10/6/2022.
//

#ifndef SRC_BASEQUESTION_H
#define SRC_BASEQUESTION_H

#include <string>
#include <vector>
#include <memory>
#include <fstream>
#include "../Answers/BaseAnswer.h"
#include "../Utils/AnswerFieldType.h"
using std::string, std::vector, std::shared_ptr;

class BaseQuestion {
protected:
    string _questionText;
    vector<shared_ptr<BaseAnswer>> _answers;
    int _selectedAnswerIndex;
public:
    BaseQuestion();
    BaseQuestion(const string& text);
    shared_ptr<BaseQuestion> AddAnswer(const BaseAnswer& newAnswer);
    virtual shared_ptr<BaseQuestion> AddAnswer(shared_ptr<BaseAnswer> newAnswer);
    vector<shared_ptr<BaseAnswer>> GetAnswers() const;
    void SetText(const string& text);
    [[nodiscard]] string GetText() const;
    void SetAnswers(const std::vector<shared_ptr<BaseAnswer>>& answers);

    virtual void Render() const;
    virtual shared_ptr<BaseAnswer> AwaitAnswer();
    [[nodiscard]] virtual std::pair<int, int> Validate() const;
    [[nodiscard]] virtual shared_ptr<BaseQuestion> GetShared() const;
    virtual void PrintRemarks() const;
    virtual string Serialize();
    [[nodiscard]] virtual AnswerFieldType GetFieldType() const;
};


#endif //SRC_BASEQUESTION_H
