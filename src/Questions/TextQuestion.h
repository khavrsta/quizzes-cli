//
// Created by stas_ on 12/6/2022.
//

#ifndef QUIZZES_TEXTQUESTION_H
#define QUIZZES_TEXTQUESTION_H

#include "BaseQuestion.h"

class TextQuestion : public BaseQuestion {
public:
    TextQuestion(const string& text);
    void Render() const override;
    std::pair<int, int> Validate() const override;
    shared_ptr<BaseAnswer> AwaitAnswer() override;
    [[nodiscard]] shared_ptr<BaseQuestion> GetShared() const override;
    void PrintRemarks() const override;
    string Serialize() override;
    [[nodiscard]] AnswerFieldType GetFieldType() const override;
    shared_ptr<BaseQuestion> AddAnswer(shared_ptr<BaseAnswer> newAnswer) override;
};


#endif //QUIZZES_TEXTQUESTION_H
