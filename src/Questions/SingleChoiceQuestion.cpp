//
// Created by stas_ on 10/6/2022.
//

#include "SingleChoiceQuestion.h"
#include <iostream>
#include "../Utils/InputManipulator.h"

using std::cout, std::endl;

SingleChoiceQuestion::SingleChoiceQuestion(const string &text)
    : BaseQuestion(text) {}

shared_ptr<BaseQuestion> SingleChoiceQuestion::GetShared() const {
    return std::make_shared<SingleChoiceQuestion>(*this);
}

void SingleChoiceQuestion::Render() const {
    BaseQuestion::Render();
    PrintRemarks();
}

shared_ptr<BaseAnswer> SingleChoiceQuestion::AwaitAnswer() {
    int choice = InputManipulator::ReadMenuChoice((int)_answers.size(), 1);
    _selectedAnswerIndex = choice - 1;
    return _answers[_selectedAnswerIndex];
}

std::pair<int, int> SingleChoiceQuestion::Validate() const {
    if (_answers[_selectedAnswerIndex]->IsCorrect()) {
        return {1, 1};
    }
    return {0, 1};
}

void SingleChoiceQuestion::PrintRemarks() const {
    cout << "You are allowed to choose only 1 answer." << endl;
}

string SingleChoiceQuestion::Serialize() {
    string data = "1 " + std::to_string(_answers.size()) + "\n" + _questionText + "\n";
    for(const auto& answer : _answers) {
        data += answer->Serialize();
    }
    return data;
}

AnswerFieldType SingleChoiceQuestion::GetFieldType() const {
    return AnswerFieldType::RadioButton;
}
