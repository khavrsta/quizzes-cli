//
// Created by stas_ on 10/6/2022.
//

#include <iostream>
#include "InfoQuestion.h"
using std::cout, std::endl;

InfoQuestion::InfoQuestion(const string& text) : BaseQuestion(text) {
    _answers.clear();
}

void InfoQuestion::Render() const {
    cout << _questionText << endl;
    PrintRemarks();
}

shared_ptr<BaseAnswer> InfoQuestion::AwaitAnswer() {
    cout << "Press Enter key to continue." << endl;
    getchar();
    return nullptr;
}

shared_ptr<BaseQuestion> InfoQuestion::GetShared() const {
    return std::make_shared<InfoQuestion>(*this);
}

void InfoQuestion::PrintRemarks() const {
    cout << "This is info question." << endl;
}

string InfoQuestion::Serialize() {
    // Info Question type is 0, and have 0 answers
    return "0 0\n" + _questionText + "\n";
}

AnswerFieldType InfoQuestion::GetFieldType() const {
    return AnswerFieldType::Info;
}
