#include <string>
#include <iostream>
#include "Application.h"

int main() {
    Application app;
    try {
        app.Run();
    } catch (const std::invalid_argument& ex) {
        std::cout << "Met critical exception:" << std::endl;
        std::cout << ex.what() << std::endl;
    }

    return EXIT_SUCCESS;
}
