//
// Created by stas_ on 10/6/2022.
//

#include "Quiz.h"
#include "Questions/InfoQuestion.h"
#include "Questions/SingleChoiceQuestion.h"
#include "Questions/MultiChoiceQuestion.h"
#include "Questions/TextQuestion.h"
#include "Answers/Validation/ValidationRule.h"
#include "Answers/TextAnswer.h"
#include "Utils/InputManipulator.h"
#include <fstream>
#include <sstream>

Quiz::Quiz() = default;
Quiz::Quiz(const string& name) : Quiz() {
    _filename = name;
}

void Quiz::RenderEntireQuiz() {
    for(const auto& question : _questions) {
        question->Render();
        question->AwaitAnswer();
        auto res = question->Validate();
        _quizScore.first += res.first;
        _quizScore.second += res.second;
    }
}

shared_ptr<Quiz> Quiz::GetShared() const {
    return std::make_shared<Quiz>(*this);
}

vector<shared_ptr<BaseQuestion>> Quiz::GetAllQuestions() const {
    return _questions;
}

shared_ptr<BaseQuestion> Quiz::GetQuestion(int index) const {
    if (index < 0 || index >= (int)_questions.size()) {
        throw std::invalid_argument("Quiz::GetQuestion(int index): Index out of range.");
    }
    return _questions[index];
}

void Quiz::SetFilename(const string& filename) {
    if (filename.empty())
        throw std::invalid_argument("Quiz::SetFilename(const string &filename): Quiz filename cannot be empty.");
    _filename = filename;
}

string Quiz::GetFilename() const {
    return _filename;
}

void Quiz::Open(const string& filename) {
    std::ifstream inFile(filename, std::ios::in);
    if (!inFile.is_open()) {
        throw std::invalid_argument("Quiz::Open(const string& filename): Such quiz file does not exist.");
    }
    int numberOfQuestions;
    if (inFile.eof()) {
        throw std::invalid_argument("Quiz::Open(const string& filename): Unexpected end of file. (missing questions)");
    }
    inFile >> numberOfQuestions;
    string line;
    for(int i = 0; i < numberOfQuestions; ++i) {
        int questionType, numberOfAnswers;
        if (!(inFile >> questionType >> numberOfAnswers)) {
            throw std::invalid_argument("Quiz::Open(const string& filename): Invalid questing options. (expected to read 2 numbers)");
        }
        line = InputManipulator::ReadStringFromFile(inFile);
        string questionText(line);
        if (questionText.empty()) {
            throw std::invalid_argument("Quiz::Open(const string& filename): Missing question text.");
        }

        shared_ptr<BaseQuestion> readQuestion;
        if (questionType == 0) {
            readQuestion = std::make_shared<InfoQuestion>(questionText);
        } else if (questionType == 1) {
            readQuestion = std::make_shared<SingleChoiceQuestion>(questionText);
        } else if (questionType == 2) {
            readQuestion = std::make_shared<MultiChoiceQuestion>(questionText);
        } else if (questionType == 3) {
            readQuestion = std::make_shared<TextQuestion>(questionText);
        } else {
            throw std::invalid_argument("Quiz::Open(const string& filename): Invalid question type.");
        }

        // Text Questions have separated logic for saving number of validation rules instead of answers
        if (questionType == 3) {
            auto textAnswer = std::make_shared<TextAnswer>(">>>");
            int numberOfRules;
            if (!(inFile >> numberOfRules)) {
                throw std::invalid_argument("Quiz::Open(const string& filename): Missing the number of text validation rules.");
            }
            for (int j = 0; j < numberOfRules; ++j) {
                if (inFile.eof()) {
                    throw std::invalid_argument("Quiz::Open(const string& filename): Missing validation rule.");
                }
                line = InputManipulator::ReadStringFromFile(inFile);
                textAnswer->AddValidationRule(line);
            }
            auto textQuestion = std::dynamic_pointer_cast<TextQuestion>(readQuestion);
            textQuestion->AddAnswer(textAnswer);
            _questions.push_back(textQuestion);
        } else {
            for(int j = 0; j < numberOfAnswers; ++j) {
                if (inFile.eof()) {
                    throw std::invalid_argument("Quiz::Open(const string& filename): Missing expected quiz answer.");
                }
                int isValidAnswer = -1;
                if (!(inFile >> isValidAnswer) || isValidAnswer < 0 || isValidAnswer > 1) {
                    throw std::invalid_argument("Quiz::Open(const string& filename): Missing/Wrong validity of quiz answer.");
                }
                line = InputManipulator::ReadStringFromFile(inFile);
                string answerText(line);
                if (questionText.empty()) {
                    throw std::invalid_argument("Quiz::Open(const string& filename): Missing answer text.");
                }
                BaseAnswer readAnswer(answerText, isValidAnswer == 1);
                readQuestion->AddAnswer(readAnswer);
            }
            _questions.push_back(readQuestion);
        }
    }
    inFile.close();
}

void Quiz::Save(const string& filename) {
    std::ofstream outFile(filename, std::ios::trunc);
    if (!outFile.good()) {
        throw std::invalid_argument("Quiz::Save(const string& filename): Failed to open file.");
    }
    outFile << _questions.size() << "\n";
    for(const auto& question : _questions) {
        outFile << question->Serialize();
    }
    outFile.close();
}

std::pair<int, int> Quiz::GetScore() const {
    return _quizScore;
}

void Quiz::SetQuestions(const vector<shared_ptr<BaseQuestion>> &questions) {
    _questions.clear();
    for(const auto& q : questions) {
        _questions.push_back(q);
    }
}