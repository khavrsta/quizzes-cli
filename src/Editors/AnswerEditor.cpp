//
// Created by stas_ on 12/6/2022.
//

#include <iostream>
#include "AnswerEditor.h"
#include "../Answers/TextAnswer.h"
#include "ValidationRuleEditor.h"
#include "../Utils/InputManipulator.h"
using std::cout, std::endl, std::cin;

AnswerEditor::AnswerEditor() {
    _type = AnswerFieldType::None;
}

void AnswerEditor::SetAnswerType(AnswerFieldType type) {
    _type = type;
}

void AnswerEditor::ShowMenu() {
    if (_type == AnswerFieldType::TextInput) {
        ValidationRuleEditor editor;
        auto answer = std::dynamic_pointer_cast<TextAnswer>(_answers[0]->GetShared());
        editor.SetItems(answer->GetAllRules());
        editor.ShowMenu();
        answer->SetValidationRules(editor.GetItems());
        return;
    }

    int maxChoice = 4;
    int choice;
    do {
        cout << "\tAnswers Editor" << endl;
        cout << "1. Show all existing answers." << endl;
        cout << "2. Create an answer." << endl;
        cout << "3. Edit an existing answer text." << endl;
        cout << "4. Delete an existing answer." << endl;
        cout << "0. Close \"Answer editor\"." << endl;
        choice = InputManipulator::ReadMenuChoice(maxChoice);

        // if no answers — then no what to show, edit or delete — only create or editor's closing
        if (_answers.empty() && choice != 0 && choice != 2) {
            cout << "The question does not have any answers yet." << endl;
            continue;
        }

        if (choice == 1) {
            int i = 0;
            for(const auto& answer : _answers) {
                cout << i++ << ". ";
                answer->Render();
            }
        } else if (choice == 2) {
            if (_type == AnswerFieldType::TextInput && !_answers.empty()) {
                cout << "Text question can have only one text field for answer!" << endl;
                continue;
            }
            Create();
        } else if (choice == 3) {
            if (_type == AnswerFieldType::TextInput && !_answers.empty()) {
                cout << "Text question answer can not have a text value!" << endl;
                continue;
            }
            cout << "Please, enter an answer number (0.." << _answers.size() - 1 << "):" << endl;
            int answerIndex = InputManipulator::ReadMenuChoice((int)_answers.size() - 1);
            Edit(answerIndex);
        } else if (choice == 4) {
            cout << "Please, enter an answer number (0.." << _answers.size() - 1 << "):" << endl;
            int answerIndex = InputManipulator::ReadMenuChoice((int)_answers.size() - 1);
            Delete(answerIndex);
        }
    } while(choice != 0);
}

void AnswerEditor::Create() {
    cout << "Enter the answer text:\n";
    string answerText = InputManipulator::ReadLine();
    auto newAnswer = std::make_shared<BaseAnswer>(answerText, true);
    bool shouldAskValidity = false;
    switch(_type) {
        case AnswerFieldType::TextInput:
            newAnswer = TextAnswer(answerText).GetShared();
            cout << "This is a Text Answer and without validation rules would be count as Correct (user may enter any value)." << endl;
            break;
        case AnswerFieldType::RadioButton:
        case AnswerFieldType::CheckBox:
            newAnswer = BaseAnswer(answerText).GetShared();
            shouldAskValidity = true;
            break;
        case AnswerFieldType::Info:
            break;
        default:
            throw std::invalid_argument("AnswerEditor::CreateAnswer(): Has been passed unknown answer type.");
            break;
    }
    if (shouldAskValidity) {
        cout << "Is this correct question answer? (enter 0 for false, 1 for true):" << endl;
        int validity = InputManipulator::ReadMenuChoice(1, 0);
        newAnswer->SetCorrectness(validity == 1);
    }
    _answers.push_back(newAnswer);
    cout << "Answer has been successfully created." << endl;
}

void AnswerEditor::Edit(int index) {
    CheckIsIndexOutOfRange(index);
    cout << "Previous text value is: \"" << _answers[index]->GetText() << "\"" << endl;
    cout << "Enter a new text:" << endl;
    cout << ">>>";
    string answerText = InputManipulator::ReadLine();
    _answers[index]->SetText(answerText);
    cout << "Answer has been successfully edited!" << endl;
}

void AnswerEditor::Delete(int index) {
    CheckIsIndexOutOfRange(index);
    _answers.erase(_answers.begin() + index);
    cout << "Answer #" << index << " has been deleted." << endl;
}

shared_ptr<AnswerEditor> AnswerEditor::GetShared() const {
    return std::make_shared<AnswerEditor>(*this);
}

void AnswerEditor::CheckIsIndexOutOfRange(int index) const {
    if (index < 0 || index >= (int)_answers.size()) {
        throw std::invalid_argument("AnswerEditor::CheckIsIndexOutOfRange(int index): Index is out of range.");
    }
}
void AnswerEditor::SetItems(const std::vector<shared_ptr<BaseAnswer>> &answers) {
    _answers.clear();
    for(const auto& answer : answers) {
        _answers.push_back(answer);
    }
}

std::vector<shared_ptr<BaseAnswer>> AnswerEditor::GetAllAnswers() const {
    return _answers;
}
