//
// Created by stas_ on 12/6/2022.
//

#ifndef QUIZZES_VALIDATIONRULEEDITOR_H
#define QUIZZES_VALIDATIONRULEEDITOR_H

#include "BaseEditor.h"
#include "../Answers/Validation/ValidationRule.h"

class ValidationRuleEditor : public BaseEditor{
private:
    std::vector<shared_ptr<ValidationRule>> _rules;
    string ReadRawRule();
    void CheckIsIndexOutOfRange(int index) const override;
public:
    void ShowMenu() override;
    void Create() override;
    void Edit(int index) override;
    void Delete(int index) override;
    [[nodiscard]] shared_ptr<ValidationRuleEditor> GetShared() const;

    void SetItems(const std::vector<ValidationRule>& rules);
    std::vector<shared_ptr<ValidationRule>> GetItems() const;
};


#endif //QUIZZES_VALIDATIONRULEEDITOR_H
