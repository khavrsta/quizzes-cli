//
// Created by stas_ on 12/6/2022.
//

#include <iostream>
#include <memory>
#include "../Utils/InputManipulator.h"
#include "QuestionEditor.h"
#include "AnswerEditor.h"
#include "../Questions/InfoQuestion.h"
#include "../Questions/SingleChoiceQuestion.h"
#include "../Questions/MultiChoiceQuestion.h"
#include "../Questions/TextQuestion.h"
using std::cout, std::cin, std::endl;

void QuestionEditor::CheckIsIndexOutOfRange(int index) const {
    if (index < 0 || index >= (int)_questions.size()) {
        throw std::invalid_argument("QuestionEditor::CheckIsIndexOutOfRange(int index): Index is out of range.");
    }
}

void QuestionEditor::ShowMenu() {
    int choice;
    do {
        cout << "\tQuestion Editor" << endl;
        cout << "1. Show all questions." << endl;
        cout << "2. Create a question." << endl;
        cout << "3. Edit the question text." << endl;
        cout << "4. Delete a question." << endl;
        cout << "5. Edit the question answers." << endl;
        cout << "0. Close \"Question Editor\"." << endl;
        choice = InputManipulator::ReadMenuChoice(5);

        // if no question — then no what to show, edit or delete — only create or editor's closing
        if (_questions.empty() && choice != 0 && choice != 2) {
            cout << "Quiz does not have any questions." << endl;
            continue;
        }

        if (choice == 1) {
            int i = 0;
            for(const auto& question : _questions) {
                cout << i++ << ". " << question->GetText() << endl;
                cout << '\t';
                question->PrintRemarks();
            }
        } else if (choice == 2) {
            Create();
        } else if (choice == 3) {
            int questionIndex = ReadMenuIndex();
            Edit(questionIndex);
        } else if (choice == 4) {
            int questionIndex = ReadMenuIndex();
            Delete(questionIndex);
        } else if (choice == 5) {
            int questionIndex = ReadMenuIndex();
            if (_questions[questionIndex]->GetFieldType() == AnswerFieldType::Info) {
                cout << "Info Questions can not have answers!" << endl;
                continue;
            }
            AnswerEditor editor;
            editor.SetItems(_questions[questionIndex]->GetAnswers());
            editor.SetAnswerType(_questions[questionIndex]->GetFieldType());
            editor.ShowMenu();
            _questions[questionIndex]->SetAnswers(editor.GetAllAnswers());
        }

    } while(choice != 0);
}

shared_ptr<QuestionEditor> QuestionEditor::GetShared() const {
    return std::make_shared<QuestionEditor>(*this);
}

void QuestionEditor::SetItems(const std::vector <BaseQuestion>& questions) {
    _questions.clear();
    for(const auto& question : questions) {
        _questions.push_back(question.GetShared());
    }
}

void QuestionEditor::Edit(int index) {
    CheckIsIndexOutOfRange(index);
    cout << "Previous text value is: \"" << _questions[index]->GetText() << "\"" << endl;
    cout << "Enter a new text:" << endl;
    cout << ">>>";
    string questionText = InputManipulator::ReadLine();
    _questions[index]->SetText(questionText);
    cout << "Question has been successfully edited!" << endl;
}

void QuestionEditor::Delete(int index) {
    CheckIsIndexOutOfRange(index);
    _questions.erase(_questions.begin() + index);
    cout << "Question #" << index << " has been deleted." << endl;
}

int QuestionEditor::ReadMenuIndex() const {
    cout << "Please, enter a question number (0.." << _questions.size() - 1 << "):" << endl;
    return InputManipulator::ReadMenuChoice((int)_questions.size() - 1);
}

void QuestionEditor::Create() {
    cout << "Enter the question text:\n";
    string questionText = InputManipulator::ReadLine();
    auto newQuestion = ChooseQuestionType(questionText);
    _questions.push_back(newQuestion);
    cout << "Question has been successfully created." << endl;
}

shared_ptr<BaseQuestion> QuestionEditor::ChooseQuestionType(const string& questionText) {
    cout << "Choose a question type:" << endl;
    cout << "1. Info / Message box." << endl;
    cout << "2. Single choice." << endl;
    cout << "3. Multi choice." << endl;
    cout << "4. Text field." << endl;
    int choice = InputManipulator::ReadMenuChoice(4, 1);
    if (choice == 1)
        return std::make_shared<InfoQuestion>(questionText);
    else if (choice == 2)
        return std::make_shared<SingleChoiceQuestion>(questionText);
    else if (choice == 3)
        return std::make_shared<MultiChoiceQuestion>(questionText);
    else
        return std::make_shared<TextQuestion>(questionText);
}

std::vector<shared_ptr<BaseQuestion>> QuestionEditor::GetQuestions() const {
    return _questions;
}

void QuestionEditor::SetItems(const std::vector<shared_ptr<BaseQuestion>>& questions) {
    _questions.clear();
    for(const auto& question : questions) {
        _questions.push_back(question);
    }
}