//
// Created by stas_ on 12/6/2022.
//

#ifndef QUIZZES_QUESTIONEDITOR_H
#define QUIZZES_QUESTIONEDITOR_H

#include "../Questions/BaseQuestion.h"
#include "BaseEditor.h"

class QuestionEditor : public BaseEditor {
private:
    std::vector<shared_ptr<BaseQuestion>> _questions;
    void CheckIsIndexOutOfRange(int index) const override;
    [[nodiscard]] int ReadMenuIndex() const;
    static shared_ptr<BaseQuestion> ChooseQuestionType(const string& questionText);
public:
    void ShowMenu() override;
    void Create() override;
    void Edit(int index) override;
    void Delete(int index) override;
    [[nodiscard]] shared_ptr<QuestionEditor> GetShared() const;
    void SetItems(const std::vector<BaseQuestion>& questions);
    void SetItems(const std::vector<shared_ptr<BaseQuestion>>& questions);
    [[nodiscard]] std::vector<shared_ptr<BaseQuestion>> GetQuestions() const;
};


#endif //QUIZZES_QUESTIONEDITOR_H
