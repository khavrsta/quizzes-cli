//
// Created by stas_ on 12/6/2022.
//

#include <iostream>
#include "ValidationRuleEditor.h"
#include "../Utils/InputManipulator.h"

using std::cout, std::endl, std::cin;

void ValidationRuleEditor::ShowMenu() {
    int choice;
    do {
        cout << "\tValidation Rules Editor" << endl;
        cout << "1. Show all validation rules." << endl;
        cout << "2. Create a new validation rule." << endl;
        cout << "3. Edit a existing validation rule." << endl;
        cout << "4. Delete a existing validation rule." << endl;
        cout << "0. Close \"Rules Editor\"." << endl;
        choice = InputManipulator::ReadMenuChoice(4);
        if (choice == 1) {
            int i = 0;
            for(const auto& rule : _rules) {
                cout << i++ << ". ";
                rule->Show();
            }
            getchar();
        } else if (choice == 2) {
            Create();
        } else if (choice == 3) {
            if (_rules.empty())
                break;
            cout << "Please, enter a rule number (0.." << _rules.size() - 1 << "):" << endl;
            int ruleIndex = InputManipulator::ReadMenuChoice((int)_rules.size());
            Edit(ruleIndex);
        } else if (choice == 4) {
            if (_rules.empty())
                break;
            cout << "Please, enter a rule number (0.." << _rules.size() - 1 << "):" << endl;
            int ruleIndex = InputManipulator::ReadMenuChoice((int)_rules.size());
            Delete(ruleIndex);
        }
    } while (choice != 0);
}

void ValidationRuleEditor::Create() {
    cout << "Please, enter a validation rule in the format \"<operation>:<value>\"" << endl;
    cout << "Acceptable operations are:" << endl;
    cout << "<, >, !=, ==, <=, >=, match, contains" << endl;
    cout << "Examples:" << endl;
    cout << ">=:30" << endl;
    cout << "contains:red" << endl;
    string rawRule = ReadRawRule();
    cout << "Validation rule has been successfully created!" << endl;
}

void ValidationRuleEditor::Edit(int index) {
    CheckIsIndexOutOfRange(index);
    cout << "Previous value is: \"" << _rules[index] << "\"" << endl;
    cout << "Enter a new value:" << endl;
    cout << ">>>";
    ReadRawRule();
    cout << "Validation rule has been successfully edited!" << endl;
}

void ValidationRuleEditor::Delete(int index) {
    CheckIsIndexOutOfRange(index);
    _rules.erase(_rules.begin() + index);
    cout << "Validation rule #" << index << " has been deleted." << endl;
}

shared_ptr<ValidationRuleEditor> ValidationRuleEditor::GetShared() const {
    return std::make_shared<ValidationRuleEditor>(*this);
}

string ValidationRuleEditor::ReadRawRule() {
    string rawRule;
    bool isValid = false;
    do {
        rawRule = InputManipulator::ReadLine();
        try {
            // ctor contains parsing validation from the rawRule
            ValidationRule newRule(rawRule);
            isValid = true;
            _rules.push_back(newRule.GetShared());
            break;
        } catch (const std::invalid_argument &ex) {
            cout << "Failed to create rule. Met a problem:" << endl;
            cout << ex.what() << endl;
            isValid = false;
            continue;
        }
    } while (!isValid);
    return rawRule;
}

void ValidationRuleEditor::CheckIsIndexOutOfRange(int index) const {
    if (index < 0 || index >= (int)_rules.size()) {
        throw std::invalid_argument("ValidationRuleEditor::CheckIsIndexOutOfRange(int index): Index is out of range.");
    }
}

void ValidationRuleEditor::SetItems(const std::vector<ValidationRule> &rules) {
    _rules.clear();
    for(const auto& rule : rules) {
        _rules.push_back(rule.GetShared());
    }
}

std::vector<shared_ptr<ValidationRule>> ValidationRuleEditor::GetItems() const {
    return _rules;
}
