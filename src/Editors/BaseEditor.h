//
// Created by stas_ on 12/6/2022.
//

#ifndef QUIZZES_BASEEDITOR_H
#define QUIZZES_BASEEDITOR_H

#include <vector>
#include <memory>

using std::shared_ptr;

class BaseEditor {
protected:
    virtual void CheckIsIndexOutOfRange(int index) const;
public:
    virtual void ShowMenu() = 0;
    virtual void Create() = 0;
    virtual void Edit(int index) = 0;
    virtual void Delete(int index) = 0;
};


#endif //QUIZZES_BASEEDITOR_H
