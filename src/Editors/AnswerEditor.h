//
// Created by stas_ on 12/6/2022.
//

#ifndef QUIZZES_ANSWEREDITOR_H
#define QUIZZES_ANSWEREDITOR_H

#include <memory>
#include <vector>
#include "../Answers/BaseAnswer.h"
#include "../Utils/AnswerFieldType.h"
#include "BaseEditor.h"

using std::shared_ptr;

class AnswerEditor : public BaseEditor {
private:
    AnswerFieldType _type;
    std::vector<shared_ptr<BaseAnswer>> _answers;
    void CheckIsIndexOutOfRange(int index) const override;
public:
    AnswerEditor();
    void SetAnswerType(AnswerFieldType type);
    void Create() override;
    void Edit(int answerIndex) override;
    void Delete(int answerIndex) override;
//    void PreviewAnswer(int answerIndex) const;
//    void PreviewAllAnswers() const;
    [[nodiscard]] shared_ptr<AnswerEditor> GetShared() const;
    void ShowMenu() override;
    void SetItems(const std::vector<shared_ptr<BaseAnswer>>& answers);
    [[nodiscard]] std::vector<shared_ptr<BaseAnswer>> GetAllAnswers() const;
};


#endif //QUIZZES_ANSWEREDITOR_H
