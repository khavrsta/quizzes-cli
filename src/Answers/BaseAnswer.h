//
// Created by stas_ on 10/6/2022.
//

#ifndef SRC_BASEANSWER_H
#define SRC_BASEANSWER_H

#include <string>
#include <memory>
using std::string, std::shared_ptr;

class BaseAnswer {
protected:
    string _answerText;
    string _strValue;
    bool _isCorrect;
    bool _hasSelected;
public:
    BaseAnswer(const string& text);
    BaseAnswer(const string& text, bool isCorrect);
    [[nodiscard]] string GetText() const;
    void SetText(const string& text);
    [[nodiscard]] bool IsSelected() const;
    void Select();
    void SetCorrectness(bool isValid);

    [[nodiscard]] virtual bool IsCorrect() const;
    [[nodiscard]] virtual string GetStringValue() const;
    [[nodiscard]] virtual shared_ptr<BaseAnswer> GetShared() const;
    virtual void Render() const;
    virtual string Serialize();
};


#endif //SRC_BASEANSWER_H
