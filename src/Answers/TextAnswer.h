//
// Created by stas_ on 12/6/2022.
//

#ifndef QUIZZES_TEXTANSWER_H
#define QUIZZES_TEXTANSWER_H

#include <vector>
#include <memory>
#include "BaseAnswer.h"
#include "Validation/ValidationRule.h"

using std::shared_ptr;

class TextAnswer : public BaseAnswer {
private:
    std::vector<ValidationRule> _rules;
public:
    TextAnswer(const string& text);
    void AddValidationRule(const string& rule);
    void RemoveValidationRule(int ruleIndex);
    void ShowAllValidationRules() const;
    void SetValue(const string& value);
    [[nodiscard]] std::vector<ValidationRule> GetAllRules() const;
    void SetValidationRules(const std::vector<shared_ptr<ValidationRule>>& rules);

    [[nodiscard]] bool IsCorrect() const override;
    [[nodiscard]] string GetStringValue() const override;
    [[nodiscard]] shared_ptr<BaseAnswer> GetShared() const override;
    string Serialize() override;
};


#endif //QUIZZES_TEXTANSWER_H
