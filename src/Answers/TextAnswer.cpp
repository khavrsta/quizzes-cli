//
// Created by stas_ on 12/6/2022.
//

#include <iostream>
#include "TextAnswer.h"

TextAnswer::TextAnswer(const string& text) : BaseAnswer(text) {
    _isCorrect = false;
    _hasSelected = true;
}

void TextAnswer::AddValidationRule(const string &rule) {
    _rules.emplace_back(rule);
}

void TextAnswer::RemoveValidationRule(int ruleIndex) {
    if (ruleIndex < 0 || ruleIndex >= (int)_rules.size()) {
        throw std::invalid_argument("TextAnswer::RemoveValidationRule(int ruleIndex): Rule index is out of range.");
    }
    _rules.erase(_rules.begin() + ruleIndex);
}

void TextAnswer::ShowAllValidationRules() const {
    if (_rules.empty()) {
        std::cout << "No validation rules (any input text is allowed)" << std::endl;
        return;
    }
    std::cout << "\tThis text answer must comply with the following rules:\n";
    int i = 0;
    for(const auto& rule : _rules) {
        std::cout << '\t' << i++ << ". ";
        rule.Show();
    }
}

bool TextAnswer::IsCorrect() const {
    for(const auto& rule : _rules) {
        if (!rule.IsValidValue(_strValue)) {
            return false;
        }
    }
    return true;
}

string TextAnswer::GetStringValue() const {
    return _strValue;
}

shared_ptr<BaseAnswer> TextAnswer::GetShared() const {
    return std::make_shared<TextAnswer>(*this);
}

void TextAnswer::SetValue(const string& value) {
    _strValue = value;
}

std::vector<ValidationRule> TextAnswer::GetAllRules() const {
    return _rules;
}

string TextAnswer::Serialize() {
    string data = std::to_string(_rules.size()) + "\n";
    for(const auto& rule : _rules) {
        data += rule.Serialize();
    }
    return data;
}

void TextAnswer::SetValidationRules(const std::vector<shared_ptr<ValidationRule>>& rules) {
    _rules.clear();
    for(const auto& rule : rules) {
        _rules.push_back(*rule);
    }
}
