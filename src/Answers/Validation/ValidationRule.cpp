//
// Created by stas_ on 12/6/2022.
//

#include <iostream>
#include <climits>
#include <string>
#include <sstream>
#include <regex>
#include "../../Application.h"
#include "ValidationRule.h"

ValidationRule::ValidationRule(const string &rawRuleString) {
    if (rawRuleString.find(ValidationRule::Separator) == string::npos) {
        throw std::invalid_argument(Application::GetFormattedErrorMessage(
                "%s:\n\"%s\"\n",
                "ValidationRule::ValidationRule(const string &rawRuleString): Has been passed invalid rule string:",
                rawRuleString.c_str()));
    }
    string less = "<";
    less += ValidationRule::Separator;
    string more = ">";
    more += ValidationRule::Separator;
    int numOfSkipChars = 1;
    if (rawRuleString.rfind("<=", 0) == 0) {
        _type = ValidationRuleType::LessOrEqual;
        numOfSkipChars = 2;
    } else if (rawRuleString.rfind(">=", 0) == 0) {
        _type = ValidationRuleType::More;
        numOfSkipChars = 2;
    } else if (rawRuleString.rfind("==", 0) == 0) {
        _type = ValidationRuleType::Equal;
        numOfSkipChars = 2;
    } else if (rawRuleString.rfind("!=", 0) == 0) {
        _type = ValidationRuleType::NotEqual;
        numOfSkipChars = 2;
    } else if (rawRuleString.rfind(less, 0) == 0) {
        _type = ValidationRuleType::Less;
    } else if (rawRuleString.rfind(more, 0) == 0) {
        _type = ValidationRuleType::More;
    } else if (rawRuleString.rfind("contains", 0) == 0) {
        _type = ValidationRuleType::Contains;
        numOfSkipChars = 8;
    } else if (rawRuleString.rfind("pattern", 0) == 0) {
        _type = ValidationRuleType::Pattern;
        numOfSkipChars = 7;
    } else {
        throw std::invalid_argument(Application::GetFormattedErrorMessage(
                "%s:\n\"%s\"\n",
                "ValidationRule::ValidationRule(const string &rawRuleString): Has been passed unknown criterion:",
                rawRuleString.c_str()));
    }

    if (!rawRuleString.substr(numOfSkipChars).rfind(ValidationRule::Separator, 0) == 0) {
        throw std::invalid_argument(Application::GetFormattedErrorMessage(
                "%s:\n\"%s\"\n",
                "ValidationRule::ValidationRule(const string &rawRuleString): Missing criterion-value separator:",
                rawRuleString.c_str()));
    }
    _ruleValue = rawRuleString.substr(numOfSkipChars + 1);
    _isNumeric = false;
    if (ConvertToInt(_ruleValue, _numericValue)) {
        _isNumeric = true;
    }
}

string ValidationRule::ToLower(string& str) {
    transform(str.begin(), str.end(), str.begin(), ::tolower);
    return str;
}

bool ValidationRule::IsValidValue(const string &value) const {
    int numericValue = 0;
    bool isNumber = ConvertToInt(value, numericValue);
    switch (_type) {
        case ValidationRuleType::Equal:
            if (isNumber && _isNumeric) {
                return numericValue == _numericValue;
            } else {
                string loweredValue(value);
                string loweredRuleValue(_ruleValue);
                return ToLower(loweredValue) == ToLower(loweredRuleValue);
            }
            break;
        case ValidationRuleType::NotEqual:
            if (isNumber && _isNumeric) {
                return numericValue != _numericValue;
            } else {
                string loweredValue(value);
                string loweredRuleValue(_ruleValue);
                return ToLower(loweredValue) == ToLower(loweredRuleValue);
            }
            break;
        case ValidationRuleType::Less:
            if (isNumber && _isNumeric)
                return numericValue < _numericValue;
            return false;
            break;
        case ValidationRuleType::More:
            if (isNumber && _isNumeric)
                return numericValue > _numericValue;
            return false;
            break;
        case ValidationRuleType::LessOrEqual:
            if (isNumber && _isNumeric)
                return numericValue <= _numericValue;
            return false;
            break;
        case ValidationRuleType::MoreOrEqual:
            if (isNumber && _isNumeric)
                return numericValue >= _numericValue;
            return false;
            break;
        case ValidationRuleType::Contains: {
            string regexStr = "\\s*" + _ruleValue + "\\s*";
            return std::regex_search(value, std::regex(regexStr));
            break;
        }
        case ValidationRuleType::Pattern: {
            return std::regex_search(value, std::regex(_ruleValue));
            break;
        }
        default:
            throw std::invalid_argument(Application::GetFormattedErrorMessage(
                    "%s:\n",
                    "ValidationRule::IsValidValue(const string &value): Met an unknown criterion:"));
            break;
    }
}

bool ValidationRule::ConvertToInt(const string &str, int& num) {
    std::stringstream ss(str);
    int number = INT32_MIN;
    if (ss >> number) {
        num = number;
        return true;
    }
    return false;
}

void ValidationRule::Show() const {
    std::cout << "Value should ";

    switch (_type) {
        case ValidationRuleType::Equal:
            std::cout << "be equal/match to \"" << _ruleValue << "\"" << std::endl;
            break;
        case ValidationRuleType::NotEqual:
            std::cout << "not be equal/match to \"" << _ruleValue << "\"" << std::endl;
            break;
        case ValidationRuleType::Less:
            std::cout << "be less (<) than \"" << _ruleValue << "\"" << std::endl;
            break;
        case ValidationRuleType::More:
            std::cout << "be more (>) than \"" << _ruleValue << "\"" << std::endl;
            break;
        case ValidationRuleType::LessOrEqual:
            std::cout << "be less or equal (<=) than \"" << _ruleValue << "\"" << std::endl;
            break;
        case ValidationRuleType::MoreOrEqual:
            std::cout << "be more oe equal (>=) than \"" << _ruleValue << "\"" << std::endl;
            break;
        case ValidationRuleType::Contains:
            std::cout << "contains the \"" << _ruleValue << "\"" << std::endl;
            break;
        case ValidationRuleType::Pattern:
            std::cout << "match to the pattern: \"" << _ruleValue << "\"" << std::endl;
            break;
        default:
            throw std::invalid_argument(Application::GetFormattedErrorMessage(
                    "%s:\n",
                    "ValidationRule::Show(): Met an unknown criterion:"));
            break;
    }
}

shared_ptr<ValidationRule> ValidationRule::GetShared() const {
    return std::make_shared<ValidationRule>(*this);
}

string ValidationRule::Serialize() const {
    return GetRawRule() + "\n";
}

string ValidationRule::GetRawRule() const {
    string data;
    switch (_type) {
        case ValidationRuleType::Equal:
            data += "==";
            break;
        case ValidationRuleType::NotEqual:
            data += "!=";
            break;
        case ValidationRuleType::Less:
            data += "<";
            break;
        case ValidationRuleType::More:
            data += ">";
            break;
        case ValidationRuleType::LessOrEqual:
            data += "<=";
            break;
        case ValidationRuleType::MoreOrEqual:
            data += ">=";
            break;
        case ValidationRuleType::Contains:
            data += "contains";
            break;
        case ValidationRuleType::Pattern:
            data += "pattern";
            break;
        default:
            throw std::invalid_argument(Application::GetFormattedErrorMessage(
                    "%s:\n",
                    "ValidationRule::GetRawRule(): Met an unknown criterion:"));
            break;
    }

    data += Separator;
    data += _ruleValue;
    return data;
}
