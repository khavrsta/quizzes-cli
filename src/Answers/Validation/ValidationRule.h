//
// Created by stas_ on 12/6/2022.
//

#ifndef QUIZZES_VALIDATIONRULE_H
#define QUIZZES_VALIDATIONRULE_H

#include <string>
#include <vector>
#include <memory>
#include "../../Utils/ValidationRuleType.h"

using std::string, std::shared_ptr;

class ValidationRule {
private:
    static const char Separator = ':';
    ValidationRuleType _type;
    string _ruleValue;
    int _numericValue;
    bool _isNumeric;
    static string ToLower(string& str);
    static bool ConvertToInt(const string& str, int& num);
public:
    ValidationRule(const string& rawRuleString);
    bool IsValidValue(const string& value) const;
    void Show() const;
    shared_ptr<ValidationRule> GetShared() const;
    string GetRawRule() const;
    string Serialize() const;
};


#endif //QUIZZES_VALIDATIONRULE_H
