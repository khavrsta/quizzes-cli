//
// Created by stas_ on 10/6/2022.
//

#include <iostream>
#include "BaseAnswer.h"
using std::cout, std::endl;

BaseAnswer::BaseAnswer(const string &text, bool isCorrect) {
    _answerText = text;
    _isCorrect = isCorrect;
    _hasSelected = false;
}

bool BaseAnswer::IsCorrect() const {
    return _isCorrect;
}

string BaseAnswer::GetText() const {
    return _answerText;
}

void BaseAnswer::Render() const {
    cout << '\t' << _answerText << endl;
}

shared_ptr<BaseAnswer> BaseAnswer::GetShared() const {
    return std::make_shared<BaseAnswer>(*this);
}

bool BaseAnswer::IsSelected() const {
    return _hasSelected;
}

void BaseAnswer::Select() {
    _hasSelected = true;
}

string BaseAnswer::GetStringValue() const {
    return _strValue;
}

BaseAnswer::BaseAnswer(const string &text) {
    _answerText = text;
    _isCorrect = false;
    _hasSelected = false;
}

void BaseAnswer::SetText(const string &text) {
    _answerText = text;
}

string BaseAnswer::Serialize() {
    string data = (_isCorrect) ? "1" : "0";
    data += "\n" + _answerText + "\n";
    return data;
}

void BaseAnswer::SetCorrectness(bool isValid) {
    _isCorrect = isValid;
}
