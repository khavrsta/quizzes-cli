//
// Created by stas_ on 12/6/2022.
//

#include <iostream>
#include <filesystem>
#include <cstdarg>
#include "Utils/InputManipulator.h"
#include "Application.h"
using std::cout, std::cin, std::endl;

Application::Application() = default;

string Application::GetFormattedErrorMessage(const char* format, ...) {
    auto temp = vector<char> {};
    auto length = size_t {63};
    va_list args;
    while (temp.size() <= length)
    {
        temp.resize(length + 1);
        va_start(args, format);
        const auto status = std::vsnprintf(temp.data(), temp.size(), format, args);
        va_end(args);
        if (status < 0)
            throw std::runtime_error {"GetFormattedErrorMessage(): String formatting error."};
        length = static_cast<size_t>(status);
    }
    return string {temp.data(), length};
}

void Application::ShowMainMenu() {
    int choice;
    do {
        cout << "\tMain Menu" << endl;
        cout << "1. Open&Start a quiz." << endl;
        cout << "2. Create a quiz." << endl;
        cout << "3. Edit a quiz." << endl;
        cout << "4. Delete a quiz." << endl;
        cout << "0. Exit" << endl;
        choice = InputManipulator::ReadMenuChoice(4);

        try {
            if (choice == 1) {
                LaunchQuiz();
            } else if (choice == 2) {
                CreateQuiz();
            } else if (choice == 3) {
                EditQuiz();
            } else if (choice == 4) {
                DeleteQuiz();
            }
        } catch (const std::invalid_argument& ex) {
            std::cout << "Caught an exception:\n";
            std::cout << ex.what() << endl;
        }
    } while(choice != 0);
}

void Application::LaunchQuiz() {
    cout << "Enter a quiz filename:" << endl;
    string filename = InputManipulator::ReadFilename();
    _currentQuiz.Open(InputManipulator::SavesDirectory + "/" + filename);
    _currentQuiz.RenderEntireQuiz();
    auto score = _currentQuiz.GetScore();
    cout << "You have earned " << score.first << " pts. / " << score.second << " pts." << endl;
    getchar();
}

void Application::CreateQuiz() {
    cout << "Enter a quiz filename:" << endl;
    string filename = InputManipulator::ReadFilename(true);
    _currentQuiz = Quiz(filename);

    if (InputManipulator::DoesSuchDirectoryExists(InputManipulator::SavesDirectory)) {
        std::filesystem::create_directory(InputManipulator::SavesDirectory);
    }

    std::ofstream outFile(InputManipulator::SavesDirectory + "/" + filename);
    outFile << "0"; // number of current questions in the new quiz equals to zero
    outFile.close();
    cout << "Quiz \"" << filename << "\" has been successfully created." << endl;
    cout << "Now you can edit it by filename, for adding questions and answers!" << endl;
}
void Application::EditQuiz() {
    cout << "Enter a quiz filename to edit:" << endl;
    string filename = InputManipulator::ReadFilename();
    _currentQuiz.Open(InputManipulator::SavesDirectory + "/" + filename);
    _currentQuiz.SetFilename(filename);
    _editor.SetItems(_currentQuiz.GetAllQuestions());
    cout << "[Attention] Quiz will be saved only when you leave Quiz Editor menu." << endl;
    _editor.ShowMenu();
    _currentQuiz.SetQuestions(_editor.GetQuestions());
    _currentQuiz.Save(InputManipulator::SavesDirectory + "/" + filename);
    cout << "[Saved] Quiz has been successfully saved." << endl;
}
void Application::DeleteQuiz() {
    cout << "Enter a quiz filename to delete:" << endl;
    string filename = InputManipulator::ReadFilename();
    std::filesystem::remove(InputManipulator::SavesDirectory + "/" + filename);
    cout << "Quiz file has been deleted." << endl;
}
void Application::Run() {
    ShowMainMenu();
}
