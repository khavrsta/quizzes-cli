//
// Created by stas_ on 12/6/2022.
//

#ifndef QUIZZES_APPLICATION_H
#define QUIZZES_APPLICATION_H

#include <memory>
#include "Quiz.h"
#include "Editors/QuestionEditor.h"

class Application {
private:
    Quiz _currentQuiz;
    QuestionEditor _editor;
public:
    Application();
    void LaunchQuiz();
    void CreateQuiz();
    void EditQuiz();
    static void DeleteQuiz();
    void Run();
    void ShowMainMenu();

    static string GetFormattedErrorMessage(const char* format, ...);
};


#endif //QUIZZES_APPLICATION_H
